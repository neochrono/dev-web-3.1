package utfpr.ct.dainf.if6ae.pratica;

import java.io.IOException;
import java.util.Date;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet(name = "Login Servlet", urlPatterns = {"/login"})
public class LoginServlet extends HttpServlet {

	private static final long serialVersionUID = 4514351050202063028L;

	protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
		String login = request.getParameter("login");
		String senha = request.getParameter("senha");
		String perfil = request.getParameter("perfil");
		switch (perfil) {
		case "1":
			perfil = "Cliente";
			break;
		case "2":
			perfil = "Gerente";
			break;
		case "3":
			perfil = "Administrador";
			break;
		default:
			break;
		}
		if(login.equals(senha)) {
			request.setAttribute("span", String.format("<span style=\"color: blue\">%s, login bem sucedido, para %s às %tT</span>", perfil, login, new Date()));
	 	} else {
	 		request.setAttribute("span", "<span style=\"color: red; font-style: italic;\">Acesso negado</span>");
	 	}
		getServletContext().getRequestDispatcher("/login.jsp").forward(request, response);
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    public String getServletInfo() {
        return "Exemplo de servlet simples";
    }
}
