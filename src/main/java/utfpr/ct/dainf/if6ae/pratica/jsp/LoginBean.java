package utfpr.ct.dainf.if6ae.pratica.jsp;

public class LoginBean {
	private String login;
	private String senha;
	private String perfil;
	public String getLogin() {
		return login;
	}
	public void setLogin(String login) {
		this.login = login;
	}
	public String getSenha() {
		return senha;
	}
	public void setSenha(String senha) {
		this.senha = senha;
	}
	public String getPerfil() {
		return perfil;
	}
	public void setPerfil(String perfil) {
		this.perfil = perfil;
	}
	@Override
	public String toString() {
		return "LoginBean [login=" + login + ", senha=" + senha + ", perfil="
				+ perfil + "]";
	}
}
